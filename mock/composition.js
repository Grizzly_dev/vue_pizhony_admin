// import Mock from 'mockjs'

const composition = [
  {
    id: 1111,
    title: 'component_1'
  },
  {
    id: 1112,
    title: 'component_2'
  },
  {
    id: 1113,
    title: 'component_3'
  },
  {
    id: 1114,
    title: 'component_4'
  }
]

export default [
  {
    url: '/composition/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: composition
      }
    }
  }
]