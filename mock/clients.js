import Mock from 'mockjs'

const List = []
const count = 5000


for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    card_id: Mock.Random.integer(0, 999),
    created: Mock.Random.datetime(),
    sex: (function () {
      if ( parseInt(Math.random()*100) %2 === 0 ){
        return 'male'
      } else {
        return 'female'
      }
    }),
    country: Mock.Random.word(7),
    city: Mock.Random.word(5),
    postIdx: Mock.Random.zip(),
    postAddr: Mock.Random.city(),
    name: '@first',
    surname: '@last',
    "recommend": ["user 3 | +375 33 667-54-33"],
    comment: Mock.Random.word(15),
    found_out: Mock.Random.word(7),
    birthDate: Mock.Random.datetime(),
    wedd_date: Mock.Random.now('day', 'yyyy-MM-dd')+'T'+Mock.Random.now('day', 'HH:mm:ss.SS')+'Z',
    "tags_main": ["user tag 2", "another tag"],
    "tags_client": ["tag1", "tag2", "tag3231"],
    shirtSize: [],
    "trouserSize": ["XL", "M"],
    shoeSize: [],
    "images": {
      "main_img": (()=>{
        if ( parseInt((Math.random()*22))%2 == 0 ){
          return {
            id: 0,
            src: "https://picsum.photos/1024/620/?image="+Mock.Random.integer(1, 140),
          }
        } else {
          return null
        }
      }),
      "other": [
        {
          id: 1,
          src: "https://picsum.photos/1024/620/?image="+Mock.Random.integer(1, 140)
        },
        {
          id: 2,
          src: "https://picsum.photos/1024/620/?image="+Mock.Random.integer(1, 140)
        },
        {
          id: 3,
          src: "https://picsum.photos/1024/620/?image="+Mock.Random.integer(1, 140)
        },
        {
          id: 4,
          src: "https://picsum.photos/1024/620/?image="+Mock.Random.integer(1, 140)
        }
        ]
    },
    "rate": Mock.Random.integer(0, 4),
    email: Mock.Random.email(),
    "position": [Mock.Random.word(5)],
    phone: '+' + Mock.Random.integer(375170000000, 375449999999),
    discount: Mock.Random.integer(0, 33),
    cashback: Mock.Random.integer(0, 3000),
    total: Mock.Random.integer(0, 99999)
  }))
}

export default [
  {
    url: '/clients/list',
    type: 'get',
    response: config => {
      const {name, email, card_id, phone, birthDate, recommend, page = 1, limit, sort} = config.query

      let mockList = List.filter(item => {
        if (card_id && item.card_id !== +card_id) return false
        if (phone && item.phone !== phone) return false
        if (name && item.name !== name) return false
        if (birthDate && item.birthDate !== birthDate) return false
        if (email && item.email !== email) return false
        if (recommend && item.recommend !== recommend) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      var pageList = mockList
      if (limit){
        pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))
      } else {
        pageList = mockList
      }

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  {
    url: '/clients/filter',
    type: 'get',
    response: config => {
      const query = config.query[0]

      let mockList = List.filter(item => {
        // if (query && item.card_id == +query) return true
        if (query && item.phone.includes(query)) return true
        if (query && item.name.includes(query)) return true
        if (query && item.surname.includes(query)) return true
        return false
      })

      var pageList = mockList

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  {
    url: '/clients/detail',
    type: 'get',
    response: config => {
      const {id} = config.query
      for (const article of List) {
        if (article.id === +id) {
          return {
            code: 20000,
            data: article
          }
        }
      }
    }
  },

  {
    url: '/clients/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            {key: 'PC', pv: 1024},
            {key: 'mobile', pv: 1024},
            {key: 'ios', pv: 1024},
            {key: 'android', pv: 1024}
          ]
        }
      }
    }
  },

  {
    url: '/clients/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/clients/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]

