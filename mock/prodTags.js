// import Mock from 'mockjs'

const prodTags = [
  {
    id: 1011,
    title: 'tag1'
  },
  {
    id: 1012,
    title: 'tag2'
  },
  {
    id: 1013,
    title: 'tag3'
  },
  {
    id: 1014,
    title: 'tag4'
  },
  {
    id: 1015,
    title: 'tag5'
  }
]

export default [
  {
    url: '/prodtags/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: prodTags
      }
    }
  },
  {
    url: '/prodtags/detail',
    type: 'get',
    response: config => {

      var responseArr = []
      const {idArr} = config.query
      if (!idArr || !idArr.length){
        return {
          code: 20000,
          data: []
        }
      }
      for(let i=0; i<idArr.length; i++) {
        let id = idArr[i];
        for (const tag of prodTags) {
          if (tag.id === +id) {
            responseArr.push(tag)
          }
        }
      }
      return {
        code: 20000,
        data: responseArr
      }

    }
  }
]