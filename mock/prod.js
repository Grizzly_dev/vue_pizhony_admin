import Mock from 'mockjs'

const List = []
const count = 9999

for (let i = 0; i < count; i++) {
  let imgNumber = Mock.Random.integer(0,155);
  List.push(Mock.mock({
    id: i,
    name: Mock.Random.word(),
    alias: Mock.Random.word(),
    isPublished: Mock.Random.boolean(),
    isNew: false,
    isSale: false,
    category: (()=>{
      let rand = Math.floor(Math.random()*10)
      return rand <= 3 ? [111, 113] : rand <= 6 ? [112, 111] : [113, 112];
    }),
    subcategory:(()=>{
      let rand = Math.floor(Math.random()*10)
      return rand <= 3 ? [11, 13] : rand <= 6 ? [12, 11] : [13, 12];
    }),
    img: {
      preview_img: 'https://picsum.photos/200/110/?image='+imgNumber,
      main_img: {
        id: 11,
        src: 'https://picsum.photos/1024/620/?image='+imgNumber,
      },
      other: [
        {
          id: 12,
          src: 'https://picsum.photos/1024/620/?image='+Mock.Random.integer(0,155)
        },
        {
          id: 13,
          src: 'https://picsum.photos/1024/620/?image='+Mock.Random.integer(0,155)
        },
        {
          id: 14,
          src: 'https://picsum.photos/1024/620/?image='+Mock.Random.integer(0,155)
        },
        {
          id: 15,
          src: 'https://picsum.photos/1024/620/?image='+Mock.Random.integer(0,155)
        }
      ]
    },
    article: Mock.Random.integer(1000,9999),
    barcode: Mock.Random.integer(0,99) + 'A' + Mock.Random.integer(10,99) + 'N' + Mock.Random.integer(55,99),
    barcodeImgURL: Mock.Random.image('200x100', '#7e7afb'),
    price: Mock.Random.integer(50,9999), // со скидкой
    priceFull: Mock.Random.integer(50,9999),
    stockPrice: Mock.Random.integer(50,9999),
    tags: [
      1011,
      1012,
      1013
    ],
    size: ['40', '41', '42'],
    quantity: Mock.Random.integer(0, 200),
    location: (()=>{
      let rand = Math.floor(Math.random()*21)
      if (rand <= 3){
        return [
          {
            id: 11,
            title: 'Независимости'
          },
          {
            id: 12,
            title: 'Склад'
          }
        ]
      } else if (rand <= 6) {
        return [
          {
            id: 12,
            title: 'Склад'
          },
          {
            id: 13,
            title: 'Силуэт'
          }
        ]
      }else if (rand <= 9) {
        return [
          {
            id: 13,
            title: 'Силуэт'
          },
          {
            id: 14,
            title: 'Риволи'
          }
        ]
      }else if (rand <= 12) {
        return [
          {
            id: 14,
            title: 'Риволи'
          }
        ]
      }else {
        return [
          {
            id: 11,
            title: 'Независимости'
          },
          {
            id: 15,
            title: 'Немига'
          }
        ]
      }
    }),
    factory: Mock.Random.integer(0, 20),
    color: (()=>{

      let rand = Math.floor(Math.random()*21);

      if (rand <= 3){
        return 'Белый'
      } else if (rand <= 6) {
        return 'Серый'
      }else if (rand <= 9) {
        return 'Черный'
      }else if (rand <= 12) {
        return 'Красный'
      }else if (rand <= 15) {
        return 'Синий'
      }else if (rand <= 18) {
        return 'Зеленый'
      }else {
        return 'Желтый'
      }
    }),
    brand: 21,
    warranty: Mock.Random.word(),
    seasons: [
      11,
      311
    ],
    composition: [
      1111,
      1112
    ],
    compositesWithParams: {},
    models: []
  }))
}


export default [
  {
    url: '/products/all',
    type: 'get',
    response: config => {
      const {prods_id = [], article, color, tags, shops, page = 1, limit, sort } = config.query

      let mockList = List.filter(item => {
        if (prods_id.length && !prods_id.includes(String(item.id))) return false
        if (article && item.article !== article) return false
        if (tags && item.tags !== tags) return false
        if (shops && item.location !== shops[0]) return false
        if (color && item.color !== color[0]) return false
        // if (size && item.size !== size) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      var pageList = mockList
      if (limit){
        pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))
      } else {
        pageList = mockList
      }

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },


  {
    url: '/products/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const prod of List) {
        if (prod.id === +id) {
          return {
            code: 20000,
            data: prod
          }
        }
      }
    }
  },
  {
    url: '/products/article',
    type: 'get',
    response: config => {
      const { article } = config.query
      let prodsByArticle = []

      for (const prod of List) {
        let articleString = String(prod.article)
        if (articleString.includes(String(article))) {
          prodsByArticle.push(prod)
        }
      }
      return {
        code: 20000,
        data: prodsByArticle
      }
    }
  },
  {
    url: '/products/shop',
    type: 'get',
    response: config => {
      const { id } = config.query;
      let prodsByShopID = [];

      for (const prod of List) {
        prod.location.map(addr => {
          if (addr.id === +id) {
            prodsByShopID.push(prod)
          }
        })
      }

      return {
        code: 20000,
        data: prodsByShopID
      }
    }
  },
  {
    url: '/products/getbyname',
    type: 'get',
    response: config => {
      const { name } = config.query;
      let prodsByTitle = [];

      for (const prod of List) {
        if (prod.name.includes(name)) {
          prodsByTitle.push(prod)
        }
      }

      return {
        code: 20000,
        data: prodsByTitle
      }
    }
  },

  {
    url: '/orders/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  },

  {
    url: '/orders/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/orders/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]
