import Mock from 'mockjs'


// три основные категории, допо поля 'additionalFields' - запилены жестко в каждую из трех основных категорий
const subcategories = []
const count = 100

for (let i = 0; i < count; i++) {
  subcategories.push(Mock.mock({
    id: i,
    parentCategory: 3, // === parent category ID
    title: Mock.Random.word(),
    link: 'catalog.html'+'?paramsToAccordSubcategory',
    slogan: Mock.Random.word()+"!",
    categoryID: (()=>{
      let rand = Math.random() * 10

      if (rand >= 3){
        return 111
      } else if (rand >= 6 ){
        return 112
      } else {
        return 113
      }
    }),
    seo: {
      title: Mock.Random.word(),
      descr: Mock.Random.word(),
      // h1: Mock.Random.word(),
      txt: Mock.Random.word(),
    },
    img_big: 'https://picsum.photos/1024/1024/?image='+i,
    img_sm: 'https://picsum.photos/600/600/?image='+i,
  }))
}

export default [
  {
    url: '/subcategories/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: subcategories
      }
    }
  },
  {
    url: '/subcategories/detail',
    type: 'get',
    response: config => {

      var responseArr = []
      const {idArr} = config.query
      if (!idArr || !idArr.length){
        return {
          code: 20000,
          data: []
        }
      }
      for(let i=0; i<idArr.length; i++) {
        let id = idArr[i];
        for (const category of subcategories) {
          if (category.id === +id) {
            responseArr.push(category)
          }
        }
      }
      return {
        code: 20000,
        data: responseArr
      }

    }
  }
]