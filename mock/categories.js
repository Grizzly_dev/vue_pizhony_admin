import Mock from 'mockjs'


// три основные категории, допо поля 'additionalFields' - запилены жестко в каждую из трех основных категорий
const categories = [
  {
    id: 111,
    title: 'Одежда',
    sizes: [
      '38',
      '43',
      '12',
      '44',
      '45',
      '46'
    ],
    models: [
      {
        id: 1,
        title: "model 1"
      },
      {
        id: 2,
        title: "model 2"
      }
    ],
    brands: [
      {
        id: 1,
        title: 'abibas'
      },
      {
        id: 2,
        title: 'Иew Balance'
      }
    ],
    seasons: [
      {
        id: 11,
        title: 'Зима'
      },
      {
        id: 12,
        title: 'Лето'
      },
      {
        id: 13,
        title: 'Демисезон'
      }
    ],
    countries: [
      {
        id: 11,
        title: "Country 11"
      },
      {
        id: 12,
        title: "Country 12"
      },
      {
        id: 13,
        title: "Country 13"
      }
    ],
    additionalFields: [
      {
        id: 11,
        title: 'Комплект',
        name: 'closes.kit',
        fieldType: 'text',
        val: null
      },
      {
        id: 12,
        title: 'Состав',
        name: 'closes.composition',
        fieldType: 'select',
        val: null
      },
      {
        id: 13,
        title: 'Покрой',
        name: 'closes.cut',
        fieldType: 'text',
        val: null
      },
      {
        id: 14,
        title: 'Фактура материала',
        name: 'closes.texture',
        fieldType: 'text',
        val: null
      }
    ],
    img: 'https://picsum.photos/1024/620/?image='+Mock.Random.integer(0,155),
    seo: {
      url: Mock.Random.domain(),
      title: 'title 111',
      describtion: "descr 111"
    }
  },
  {
    id: 112,
    title: 'Обувь',
    sizes: [
      'A12',
      'A2312',
      'XL',
      'M',
      'XS'
    ],
    models: [
      {
        id: 11,
        title: "model 11"
      },
      {
        id: 12,
        title: "model 12"
      }
    ],
    brands: [
      {
        id: 11,
        title: 'Neke'
      },
      {
        id: 12,
        title: 'Иew Neke'
      }
    ],
    seasons: [
      {
        id: 211,
        title: 'Осень'
      },
      {
        id: 212,
        title: 'Небритябрь'
      },
      {
        id: 213,
        title: 'Нэпонятный'
      }
    ],
    countries: [
      {
        id: 21,
        title: "Country 21"
      },
      {
        id: 22,
        title: "Country 22"
      },
      {
        id: 23,
        title: "Country 23"
      }
    ],
    additionalFields: [
      {
        id: 21,
        title: 'Верх',
        name: 'materials.top',
        fieldType: 'select',
        val: null
      },
      {
        id: 22,
        title: 'Подошва',
        name: 'materials.sole',
        fieldType: 'select',
        val: null
      },
      {
        id: 23,
        title: 'Стелька',
        name: 'materials.insole',
        fieldType: 'select',
        val: null
      },
      {
        id: 24,
        title: 'застежка',
        name: 'materials.clasp',
        fieldType: 'select',
        val: null
      }
    ],
    img: 'https://picsum.photos/1024/620/?image='+Mock.Random.integer(0,155),
    seo: {
      url: Mock.Random.domain(),
      title: 'title 112',
      describtion: "descr 112"
    }
  },
  {
    id: 113,
    title: 'Аксессуары',
    sizes: [
      '38',
      '39',
      '40',
      '41',
      '42',
      '43',
      '44',
      '45'
    ],
    models: [
      {
        id: 21,
        title: "model 21"
      },
      {
        id: 22,
        title: "model 22"
      }
    ],
    brands: [
      {
        id: 21,
        title: 'Big abibas'
      },
      {
        id: 22,
        title: 'chetki pacanchik'
      }
    ],
    seasons: [
      {
        id: 311,
        title: 'Весна'
      }
    ],
    countries: [
      {
        id: 31,
        title: "Country 31"
      },
      {
        id: 32,
        title: "Country 32"
      },
      {
        id: 33,
        title: "Country 33"
      }
    ],
    additionalFields: [
      {
        id: 31,
        title: 'Состав',
        name: 'tolls.composition',
        fieldType: 'text',
        val: null
      }
    ],
    img: 'https://picsum.photos/1024/620/?image='+Mock.Random.integer(0,155),
    seo: {
      url: Mock.Random.domain(),
      title: 'title 113',
      describtion: "descr 113"
    }
  }
]


const contentList = [
  {
    id: 111,
    content: "<h1>test 1</h1>"
  },
  {
    id: 112,
    content: "<h1>test 2</h1>"
  },
  {
    id: 113,
    content: "<h1>test 4</h1>"
  }
]

export default [
  {
    url: '/categories/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: categories
      }
    }
  },
  {
    url: '/categories/detail',
    type: 'get',
    response: config => {

      var responseArr = []
      const {idArr} = config.query
      if (!idArr || !idArr.length){
        return {
          code: 20000,
          data: []
        }
      }
      for(let i=0; i<idArr.length; i++) {
        let id = idArr[i];
        for (const category of categories) {
          if (category.id === +id) {
            responseArr.push(category)
          }
        }
      }
      return {
        code: 20000,
        data: responseArr
      }

    }
  },
  {
    url: '/categories/content',
    type: 'get',
    response: config => {
      const {id} = config.query

      for (let content of contentList){
        if (+content.id === +id){
          return {
            code: 20000,
            data: content
          }
        }
      }

      return {
        code: 19999,
        data: {}
      }
    }
  },
]