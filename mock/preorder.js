import Mock from 'mockjs'

const List = []
const count = 10

for (let i = 0; i < count; i++) {
    List.push(Mock.mock({
        id: '@increment',
        shop: (()=>{
            let rand = Math.floor(Math.random()*21)
            if (rand <= 3){
                return {
                    id: 11,
                    title: 'Независимости'
                }
            } else if (rand <= 6) {
                return {
                    id: 12,
                    title: 'Склад'
                }
            }else if (rand <= 9) {
                return {
                    id: 13,
                    title: 'Силуэт'
                }
            }else if (rand <= 12) {
                return {
                    id: 14,
                    title: 'Риволи'
                }
            }else {
                return {
                    id: 15,
                    title: 'Немига'
                }
            }
        }),
        product: (()=>{
            let rand = Math.floor(Math.random()*21)
            if (rand <= 3){
                return {
                    id: 1,
                    title: 'Рубашка белая',
                    size: '44',
                    price: '114'
                }
            } else if (rand <= 6) {
                return {
                    id: 2,
                    title: 'Брюки',
                    size: '44',
                    price: '175'
                }
            }else if (rand <= 9) {
                return {
                    id: 1,
                    title: 'Броги',
                    size: '39',
                    price: '100'
                }
            }else if (rand <= 12) {
                return {
                    id: 2,
                    title: 'Бабочка',
                    size: '2',
                    price: '100'
                }
            }else {
                return {
                    id: 1,
                    title: 'Костюм тройка',
                    size: '44',
                    price: '380'
                }
            }
        }),
        client: (()=>{
            let rand = Math.floor(Math.random()*21)
            if (rand <= 3){
                return {
                    id: 24,
                    name: 'Лиза',
                    surname: 'Шопоголикова'
                }
            } else if (rand <= 6) {
                return {
                    id: 25,
                    name: 'Андрей',
                    surname: 'Скряжников'
                }
            }else  {
                return {
                    id: 26,
                    name: 'Родион',
                    surname: 'Богатов'
                }
            }
        }),
        notificationMethod: (()=>{
            let rand = Math.floor(Math.random()*21)
            if (rand <= 3){
                return ['SMS', 'E-mail']
            } else if (rand <= 6) {
                return ['Viber']
            }else  {
                return ['SMS', 'Viber']
            }
        }),
        isClosed: (()=>{
            let rand = Math.floor(Math.random()*21)
            if (rand <= 12){
                return false
            }else  {
                return true
            }
        })
    }))

}

export default [
    {
        url: '/preorder/list',
        type: 'get',
        response: config => {
            const { page = 1, limit = 20, sort } = config.query

            const pageList = List.filter((item, index) => index < limit * page && index >= limit * (page - 1))

            return {
                code: 20000,
                data: {
                    total: List.length,
                    items: pageList
                }
            }
        }
    }
]