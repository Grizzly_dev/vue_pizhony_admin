import Mock from 'mockjs'

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    date: Mock.Random.datetime(),
    manager: Mock.Random.word(),
    from: {
      id: Mock.Random.integer(1, 20),
      title: Mock.Random.word(),
    },
    to: {
      id: Mock.Random.integer(1, 20),
      title: Mock.Random.word(),
    },
    prod: {
      prodID: Mock.Random.integer(1, 745834),
      prodTitle: Mock.Random.word()
    },
    quantity: Mock.Random.integer(1, 9),
  }))
}


export default [
  {
    url: '/transfer/all',
    type: 'get',
    response: config => {
      const { page = 1, limit = 20 } = config.query

      const pageList = List.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: List.length,
          items: pageList
        }
      }
    }
  }
]
