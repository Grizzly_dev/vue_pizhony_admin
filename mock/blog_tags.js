import Mock from 'mockjs'

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: i,
    title: Mock.Random.word()
  }))
}

export default [
  {
    url: '/blogtags/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: List
      }
    }
  }
]