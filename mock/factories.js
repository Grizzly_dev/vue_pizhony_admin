import Mock from 'mockjs'

const factories = []
const count = 21

for (let i = 0; i < count; i++) {
  factories.push(Mock.mock({
    id: i,
    title: Mock.Random.word()
  }))
}

export default [
  {
    url: '/factories/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: factories
      }
    }
  }
]