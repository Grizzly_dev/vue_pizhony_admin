import Mock from 'mockjs'

const count = 12
const tags = {
  userTags: [],
  aboutTags: []
}

for (let i = 0; i < count; i++) {
  tags.userTags.push(Mock.mock(
    Mock.Random.word()
  ))
  tags.aboutTags.push(Mock.mock(
    Mock.Random.word()
  ))
}


export default [
  {
    url: '/tags/detail',
    type: 'get',
    response: config => {
      const { type } = config.query
      return {
        code: 20000,
        data: tags[type]
      }
    }
  }
]