import Mock from 'mockjs'

const List = [
  {
    id: 1,
    prod_id: 100,
    size: 40,
    quantity: 12
  },
  {
    id: 2,
    prod_id: 100,
    size: 41,
    quantity: 8
  },
  {
    id: 3,
    prod_id: 100,
    size: 42,
    quantity: 17
  }
]
const count = 100

for (let i = 1; i < count; i++){
  List.push(Mock.mock({
    id: i+3,
    prod_id: 100+i,
    shop_id: Mock.Random.integer(11, 15),
    size: Mock.Random.integer(36,45),
    quantity: Mock.Random.integer(0,22)
  }))
}

  export default [
  {
    url: '/sizequantity/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: List
      }
    }
  },
  {
    url: '/sizequantity/detail',
    type: 'get',
    response: config => {

      var responseArr = []
      const {idArr} = config.query
      if (!idArr || !idArr.length){
        return {
          code: 20000,
          data: []
        }
      }
      for(let i=0; i<idArr.length; i++) {
        let id = idArr[i];
        for (const size of List) {
          if (size.prod_id === +id) {
            responseArr.push(size)
          }
        }
      }
      return {
        code: 20000,
        data: responseArr
      }

    }
  }
]