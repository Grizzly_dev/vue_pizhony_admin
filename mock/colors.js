// import Mock from 'mockjs'

const colorsList = [
  'Серый',
  'Белый',
  'Черный',
  'Красный',
  'Синий',
  'Зеленый',
  'Желтый'
]

export default [
  {
    url: '/colors/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: colorsList
      }
    }
  }
]
