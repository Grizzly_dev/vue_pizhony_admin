import Mock from 'mockjs'

const sizesList = [
  {
    id: 111,
    sizes: [
      '38',
      '43',
      '12',
      '44',
      '45',
      '46'
    ]
  },
  {
    id: 112,
    sizes: [
      'A12',
      'A2312',
      'XL',
      'M',
      'XS'
    ]
  },
  {
    id: 113,
    sizes: [
      '38',
      '39',
      '40',
      '41',
      '42',
      '43',
      '44',
      '45'
    ]
  }
]

export default [
  {
    url: '/sizes/all',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: sizesList
      }
    }
  },

  {
    url: '/sizes/detail',
    type: 'get',
    response: config => {

      var responseArr = []
      const {idArr} = config.query
      if (!idArr || !idArr.length){
        return {
          code: 20000,
          data: []
        }
      }
      for(let i=0; i<idArr.length; i++) {
        let id = idArr[i];
        for (const size of sizesList) {
          if (size.id === +id) {
            responseArr.push(size)
          }
        }
      }
      return {
        code: 20000,
        data: responseArr
      }

    }
  }

]