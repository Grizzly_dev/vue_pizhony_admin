// import Mock from 'mockjs'

const shopsList = [
  {
    id: 11,
    title: 'Все магазины'
  },
  {
    id: 12,
    title: 'Независимости'
  },
  {
    id: 13,
    title: 'Склад'
  },
  {
    id: 14,
    title: 'Силуэт'
  },
  {
    id: 15,
    title: 'Немига'
  },
  {
    id: 16,
    title: 'B2B'
  },
  {
    id: 17,
    title: 'Риволи'
  }
]

export default [
  {
    url: '/shops',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: shopsList
      }
    }
  }
]
