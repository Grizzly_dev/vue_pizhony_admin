import Mock from 'mockjs'

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    name: Mock.Random.word(),
    category: (()=>{
      let rand = Math.floor(Math.random()*10)
      return rand <= 3 ? {id:111, title: 'Рубашки'} : rand <= 6 ? {id:112, title:'Брюки'} : {id:113, title:'Футболки'};
    }),
    img: Mock.Random.image(),
    article: Mock.Random.integer(1000,9999),
    price: Mock.Random.integer(50,9999),
    quantity: Mock.Random.integer(0, 200),
    location: (()=>{
      let rand = Math.floor(Math.random()*21)
      if (rand <= 3){
        return 'Независимости'
      } else if (rand <= 6) {
        return 'Склад'
      }else if (rand <= 9) {
        return 'Силуэт'
      }else if (rand <= 12) {
        return 'Риволи'
      }else {
        return 'Немига'
      }
    }),
    color: (()=>{
      let rand = Math.floor(Math.random()*21)
      if (rand <= 3){
        return 'Белый'
      } else if (rand <= 6) {
        return 'Серый'
      }else if (rand <= 9) {
        return 'Черный'
      }else if (rand <= 12) {
        return 'Красный'
      }else if (rand <= 15) {
        return 'Синий'
      }else if (rand <= 18) {
        return 'Зеленый'
      }else {
        return 'Желтый'
      }
    })
  }))
}

export default [
  {
    url: '/orders/list',
    type: 'get',
    response: config => {
      const { title, article, color, tags, shops, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (title && item.name !== title) return false
        if (article && item.article !== article) return false
        if (tags && item.tags !== tags) return false
        if (shops && item.location !== shops[0]) return false
        if (color && item.color !== color[0]) return false
        // if (size && item.size !== size) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },

  {
    url: '/orders/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const article of List) {
        if (article.id === +id) {
          return {
            code: 20000,
            data: article
          }
        }
      }
    }
  },

  {
    url: '/orders/pv',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          pvData: [
            { key: 'PC', pv: 1024 },
            { key: 'mobile', pv: 1024 },
            { key: 'ios', pv: 1024 },
            { key: 'android', pv: 1024 }
          ]
        }
      }
    }
  },

  {
    url: '/orders/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },

  {
    url: '/orders/update',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]