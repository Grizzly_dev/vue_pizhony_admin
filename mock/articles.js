import Mock from 'mockjs'

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  let imgNumber = Mock.Random.integer(0,155);

  List.push(Mock.mock({
    id: 100+i,
    title: Mock.Random.word(),
    alias: Mock.Random.word(),
    date: Mock.Random.date(),
    views: i*imgNumber,
    tag: i,
    seo: {
      h1: Mock.Random.word(),
      title: Mock.Random.word(),
      describtion: Mock.Random.word()
    },
    txt: {
      preview: Mock.Random.paragraph( 1, 3 ),
      content: Mock.Random.paragraph( 5, 199 ),
    },
    img: {
      preview: 'https://picsum.photos/200/160/?image='+imgNumber,
      main: 'https://picsum.photos/1024/620/?image='+imgNumber,
    },
  }))
}


export default [
  {
    url: '/articles/all',
    type: 'get',
    response: config => {
      const { page = 1, limit = 20, sort } = config.query

      if (sort === '-id') {
        List.reverse()
      }
      let mockList = List
      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },


  {
    url: '/articles/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const article of List) {
        if (article.id === +id) {
          return {
            code: 20000,
            data: article
          }
        }
      }
    }
  }
]
