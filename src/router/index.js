import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
// import componentsRouter from './modules/components'
// import chartsRouter from './modules/charts'
// import tableRouter from './modules/table'
// import nestedRouter from './modules/nested'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/',
        component: () => import('@/views/index/index'),
        name: 'main',
        title: 'Главная',
        meta: { title: 'Главная', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  // {
  //   path: '/documentation',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/documentation/index'),
  //       name: 'Documentation',
  //       meta: { title: 'Documentation', icon: 'documentation', affix: true }
  //     }
  //   ]
  // },
  // {
  //   path: '/guide',
  //   component: Layout,
  //   redirect: '/guide/index',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/guide/index'),
  //       name: 'Guide',
  //       meta: { title: 'Guide', icon: 'guide', noCache: true }
  //     }
  //   ]
  // },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: 'Profile', icon: 'user', noCache: true }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/clients',
    component: Layout,
    redirect: '/clients/page',
    meta: {
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/clients/clientList'),
        name: 'clients',
        title: 'Клиенты',
        meta: { title: 'Клиенты', icon: 'peoples', affix: true }
      },
      {
        path: '/clients/add',
        component: () => import('@/views/clients/addClient'),
        name: 'addClient',
        hidden: true,
        title: 'Добавить клиента',
        meta: { title: 'Добавить клиента', icon: 'peoples', affix: false }
      },
      {
        path: '/clients/:id',
        component: () => import('@/views/clients/single/index'),
        name: 'clientEdit',
        hidden: true,
        title: 'Карточка клиента',
        meta: { title: 'Карточка клиента', icon: 'peoples', affix: false }
      },
      {
        path: '/clients/:id/history',
        component: () => import('@/views/history_purchase/index'),
        name: 'history',
        hidden: true,
        meta: { title: 'История покупок', affix: false }
      },
      {
        path: '/user/:userID/history/purchase/:itemID',
        component: () => import('@/views/history_purchase/single/index'),
        name: 'purchase_history',
        hidden: true,
        meta: { title: 'История покупок', affix: false }
      }
    ]
  },
  {
    path: '/products',
    component: Layout,
    redirect: '/products/page',
    meta: {
      roles: ['admin', 'editor', 'seo'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/products/prodList'),
        name: 'products',
        title: 'Товары',
        meta: { title: 'Товары', icon: 'theme', affix: true }
      },
      {
        path: '/products/:id',
        component: () => import('@/views/products/addProd'),
        name: 'addProd',
        hidden: true,
        meta: { title: 'Добавление товара', affix: false }
      },
      {
        path: '/products/:id?/edit',
        component: () => import('@/views/products/editProd'),
        name: 'editProd',
        hidden: true,
        meta: { title: 'Редактирование товара', affix: false }
      }
    ]
  },
  {
    path: '/transfer',
    component: Layout,
    redirect: '/transfer/page',
    meta: {
      roles: ['admin'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/products/transferProd'),
        name: 'transfer',
        title: 'Трансфер',
        meta: { title: 'Трансфер', icon: 'international', affix: true }
      }
    ]
  },
  {
    path: '/preorder',
    component: Layout,
    redirect: '/preorder/page',
    meta: {
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/preorder/preorder'),
        name: 'preorder',
        title: 'Предзаказ',
        meta: { title: 'Предзаказ', icon: 'shopping', affix: true }
      },
      {
        path: '/preorder/add',
        component: () => import('@/views/preorder/addPreorder'),
        name: 'addPreorder',
        hidden: true,
        title: 'Предзаказ',
        meta: { title: 'Добавить предзаказ', icon: 'theme', affix: true }
      }
    ]
  },
  {
    path: '/checkout',
    component: Layout,
    redirect: '/checkout/page',
    meta: {
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/checkout/checkout'),
        name: 'checkout',
        title: 'Оформить покупку',
        meta: { title: 'Оформить покупку', icon: 'money', affix: true }
      }
    ]
  },

  {
    path: '/finance',
    component: Layout,
    redirect: '/finance/page',
    meta: {
      roles: ['admin'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/finance/finance'),
        name: 'finance',
        title: 'Финансы',
        meta: { title: 'Финансы', icon: 'chart', affix: true }
      }
    ]
  },
  {
    path: '/users',
    component: Layout,
    redirect: '/users/page',
    meta: {
      roles: ['admin'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/users/users'),
        name: 'users',
        title: 'Пользователи',
        meta: { title: 'Пользователи', icon: 'people', affix: true }
      },
      {
        path: '/users/add',
        component: () => import('@/views/users/addUser'),
        name: 'addUser',
        title: 'Добавить пользователя',
        hidden: true,
        meta: { title: 'Добавить пользователя', icon: 'people', affix: true }
      },
      {
        path: '/users/:id/edit',
        component: () => import('@/views/users/editUser'),
        name: 'editUser',
        title: 'Редактировать пользователя',
        hidden: true,
        meta: { title: 'Редактировать пользователя', icon: 'people', affix: true }
      }
    ]
  },
  {
    path: '/categories',
    component: Layout,
    redirect: '/categories/page',
    meta: {
      roles: ['admin', 'seo'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/categories/categories'),
        name: 'categories',
        title: 'Категории',
        meta: { title: 'Категории', icon: 'list', affix: true }
      },
      {
        path: '/categories/add',
        component: () => import('@/views/categories/addCategory'),
        name: 'addCategory',
        hidden: true,
        title: 'Добавить категорию',
        meta: { title: 'Добавить категорию', icon: 'list', affix: true }
      }
    ]
  },
  {
    path: '/settings',
    component: Layout,
    redirect: '/settings/page',
    meta: {
      roles: ['admin'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/settings/settings'),
        name: 'settings',
        title: 'Настройки',
        meta: { title: 'Настройки', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/pages',
    component: Layout,
    redirect: '/pages/edit',
    meta: {
      roles: ['admin', 'seo'] // you can set roles in root nav
    },
    children: [
      {
        path: 'edit',
        component: () => import('@/views/pages/pages'),
        name: 'pages',
        title: 'Страницы',
        meta: { title: 'Страницы', icon: 'form', affix: true }
      },
      {
        path: '/pages/blog/:id/edit',
        component: () => import('@/views/blog/editBlog'),
        name: 'editArticle',
        hidden: true,
        meta: { title: 'Редактирование статьи', affix: false }
      },
      {
        path: '/pages/blog/create',
        component: () => import('@/views/blog/editBlog'),
        name: 'createArticle',
        hidden: true,
        meta: { title: 'Создание статьи', affix: false }
      }
    ]
  },
  {
    path: '/tasks',
    component: Layout,
    redirect: '/tasks/page',
    meta: {
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/tasks/index'),
        name: 'tasks',
        title: 'Задачи',
        meta: { title: 'Задачи', icon: 'star', affix: false }
      },
      {
        path: '/tasks/add',
        component: () => import('@/views/tasks/addTask'),
        name: 'createTask',
        title: 'Новая задача',
        hidden: true,
        meta: { title: 'Новая задача', affix: false }
      }
    ]
  },
  // {
  //   path: '/permission',
  //   component: Layout,
  //   redirect: '/permission/page',
  //   alwaysShow: true, // will always show the root menu
  //   name: 'Permission',
  //   meta: {
  //     title: 'Permission',
  //     icon: 'lock',
  //     roles: ['admin', 'editor'] // you can set roles in root nav
  //   },
  //   children: [
  //     {
  //       path: 'page',
  //       component: () => import('@/views/permission/page'),
  //       name: 'PagePermission',
  //       meta: {
  //         title: 'Page Permission',
  //         roles: ['admin'] // or you can only set roles in sub nav
  //       }
  //     },
  //     {
  //       path: 'directive',
  //       component: () => import('@/views/permission/directive'),
  //       name: 'DirectivePermission',
  //       meta: {
  //         title: 'Directive Permission'
  //         // if do not set roles, means: this page does not require permission
  //       }
  //     },
  //     {
  //       path: 'role',
  //       component: () => import('@/views/permission/role'),
  //       name: 'RolePermission',
  //       meta: {
  //         title: 'Role Permission',
  //         roles: ['admin']
  //       }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/icon',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/icons/index'),
  //       name: 'Icons',
  //       meta: { title: 'Icons', icon: 'icon', noCache: true }
  //     }
  //   ]
  // },

  /** when your routing map is too long, you can split it into small modules **/
  // componentsRouter,
  // chartsRouter,
  // nestedRouter,
  // tableRouter,
  //
  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/list',
  //   name: 'Example',
  //   meta: {
  //     title: 'Example',
  //     icon: 'example'
  //   },
  //   children: [
  //     {
  //       path: 'create',
  //       component: () => import('@/views/example/create'),
  //       name: 'CreateArticle',
  //       meta: { title: 'Create Article', icon: 'edit' }
  //     },
  //     {
  //       path: 'edit/:id(\\d+)',
  //       component: () => import('@/views/example/edit'),
  //       name: 'EditArticle',
  //       meta: { title: 'Edit Article', noCache: true, activeMenu: '/example/list' },
  //       hidden: true
  //     },
  //     {
  //       path: 'list',
  //       component: () => import('@/views/example/list'),
  //       name: 'ArticleList',
  //       meta: { title: 'Article List', icon: 'list' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/tab',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/tab/index'),
  //       name: 'Tab',
  //       meta: { title: 'Tab', icon: 'tab' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/error',
  //   component: Layout,
  //   redirect: 'noRedirect',
  //   name: 'ErrorPages',
  //   meta: {
  //     title: 'Error Pages',
  //     icon: '404'
  //   },
  //   children: [
  //     {
  //       path: '401',
  //       component: () => import('@/views/error-page/401'),
  //       name: 'Page401',
  //       meta: { title: '401', noCache: true }
  //     },
  //     {
  //       path: '404',
  //       component: () => import('@/views/error-page/404'),
  //       name: 'Page404',
  //       meta: { title: '404', noCache: true }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/error-log',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'log',
  //       component: () => import('@/views/error-log/index'),
  //       name: 'ErrorLog',
  //       meta: { title: 'Error Log', icon: 'bug' }
  //     }
  //   ]
  // },

  // {
  //   path: '/excel',
  //   component: Layout,
  //   redirect: '/excel/export-excel',
  //   name: 'Excel',
  //   meta: {
  //     title: 'Excel',
  //     icon: 'excel'
  //   },
  //   children: [
  //     {
  //       path: 'export-excel',
  //       component: () => import('@/views/excel/export-excel'),
  //       name: 'ExportExcel',
  //       meta: { title: 'Export Excel' }
  //     },
  //     {
  //       path: 'export-selected-excel',
  //       component: () => import('@/views/excel/select-excel'),
  //       name: 'SelectExcel',
  //       meta: { title: 'Export Selected' }
  //     },
  //     {
  //       path: 'export-merge-header',
  //       component: () => import('@/views/excel/merge-header'),
  //       name: 'MergeHeader',
  //       meta: { title: 'Merge Header' }
  //     },
  //     {
  //       path: 'upload-excel',
  //       component: () => import('@/views/excel/upload-excel'),
  //       name: 'UploadExcel',
  //       meta: { title: 'Upload Excel' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/zip',
  //   component: Layout,
  //   redirect: '/zip/download',
  //   alwaysShow: true,
  //   name: 'Zip',
  //   meta: { title: 'Zip', icon: 'zip' },
  //   children: [
  //     {
  //       path: 'download',
  //       component: () => import('@/views/zip/index'),
  //       name: 'ExportZip',
  //       meta: { title: 'Export Zip' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/pdf',
  //   component: Layout,
  //   redirect: '/pdf/index',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/pdf/index'),
  //       name: 'PDF',
  //       meta: { title: 'PDF', icon: 'pdf' }
  //     }
  //   ]
  // },
  // {
  //   path: '/pdf/download',
  //   component: () => import('@/views/pdf/download'),
  //   hidden: true
  // },
  //
  // {
  //   path: '/theme',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/theme/index'),
  //       name: 'Theme',
  //       meta: { title: 'Theme', icon: 'theme' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/clipboard',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/clipboard/index'),
  //       name: 'ClipboardDemo',
  //       meta: { title: 'Clipboard', icon: 'clipboard' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: 'external-link',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://github.com/PanJiaChen/vue-element-admin',
  //       meta: { title: 'External Link', icon: 'link' }
  //     }
  //   ]
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
