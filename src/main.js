import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'
// components UI:
// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/ru-RU'

// Vue.use(ElementUI, { locale })

import '@/styles/index.scss' // global css
import '@/styles/custom.scss'

import App from './App'
import store from './store'
import router from './router'

import 'viewerjs/dist/viewer.css'
// use Bootstrap grid:
import 'bootstrap/scss/bootstrap-grid.scss'

import Viewer from 'v-viewer'
Vue.use(Viewer)
Vue.use(require('vue-moment'))

import './icons' // icon
// import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

// input mask:
import VueMask from 'v-mask'
Vue.use(VueMask)

// import { vsSlider } from 'vuesax'
import Vuesax from 'vuesax'
// import 'vuesax/dist/vuesax.css'
import './styles/vuesax.styl'

Vue.use(Vuesax)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */
// import { mockXHR } from '../mock'
// if (process.env.NODE_ENV === 'production') {
//   mockXHR()
// }

Vue.use(Element, {
  locale,
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

// Vue.config.errorHandler = (err, vm, info) => {
//   console.log(err, vm, info)
// };
// Global vars:
Vue.prototype.$siteBase = 'http://lovegrizzly.ga/'
