import { fetchProduct } from '@/api/prod'
import { fetchProductsList } from '@/api/prod'
import { fetchCategory } from '@/api/categories'

export default {
  state: {
    globalAllProducts: null,
    currentProduct: null,
    currentCategory: []
  },
  getters: {
    currentProduct: state => state.currentProduct,
    currentCategory: state => state.currentCategory,
    allProds: state => state.globalAllProducts
  },
  mutations: {
    setCurrentProduct: (state, data) => {
      state.currentProduct = data
    },
    setAllProducts: (state, data) => {
      state.globalAllProducts = data
    },
    setCurrentCategory: (state, data) => {
      state.currentCategory = data
    }
  },
  actions: {
    getCurrentProduct({ commit }, id) {
      return fetchProduct(id)
        .then(res => {
          commit('setCurrentProduct', res.data)
        })
    },
    getAllProducts({ commit }, query) {
      return fetchProductsList(query)
        .then(res => {
          commit('setAllProducts', res.data)
        })
    },
    getCurrentCategory({ commit }, idArr) {
      return fetchCategory(idArr)
        .then(res => {
          commit('setCurrentCategory', res.data)
        })
    }
  }
}
