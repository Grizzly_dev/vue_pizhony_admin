import { orderHistory } from './../../api/demo_data/orderHistory'

const user_history = {
  state: {
    userOrderHistory: []
  },
  getters: {
    userOrders: state => state.userOrderHistory
  },
  mutations: {
    setOrderHistory: (state, data) => { state.userOrderHistory = data }
  },
  actions: {
    getOrderHistory({ commit }) {
      return orderHistory()
        .then(res => {
          commit('setOrderHistory', res.data)
        })
    }
  }
}

export default user_history
