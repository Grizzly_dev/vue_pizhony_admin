import { fetchColors } from '@/api/colors'
import { fetchFactories } from '@/api/factories'
import { fetchProductTags } from '@/api/prodTags'

export default {
  state: {
    colors: null,
    factories: null,
    prod_tags: null
  },
  getters: {
    colors: state => state.colors,
    factories: state => state.factories,
    prod_tags: state => state.prod_tags
  },
  mutations: {
    setColors: (state, data) => { state.colors = data },
    setFactories: (state, data) => { state.factories = data },
    setProdTags: (state, data) => { state.prod_tags = data }
  },
  actions: {
    getColors({ commit }) {
      return fetchColors()
        .then(res => {
          commit('setColors', res.data)
        })
    },
    getFactories({ commit }) {
      return fetchFactories()
        .then(res => {
          commit('setFactories', res.data)
        })
    },
    getProdTags({ commit }) {
      return fetchProductTags()
        .then(res => {
          commit('setProdTags', res.data)
        })
    }
  }
}
