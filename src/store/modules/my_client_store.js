import { userTags, aboutTags, recommendList, positionList, userImages } from '@/api/demo_data/user_fields'
import { userData } from '@/api/demo_data/user'
import { fetchClientsList } from '@/api/my_client_list'

const client_store = {
  state: {
    user: {},
    selectTags: [],
    aboutTags: [],
    recommenList: [],
    positionList: [],
    profileImages: {},
    globalAllClients: null
  },
  getters: {
    user: state => state.user,
    tags: state => state.selectTags,
    aboutTags: state => state.aboutTags,
    recommends: state => state.recommenList,
    positions: state => state.positionList,
    images: state => state.profileImages,
    allClients: state => state.globalAllClients
  },
  mutations: {
    setUserData: (state, data) => { state.user = data },
    setSelectTags: (state, data) => { state.selectTags = data },
    setAboutTags: (state, data) => { state.aboutTags = data },
    setRecommends: (state, data) => { state.recommenList = data },
    setPositions: (state, data) => { state.positionList = data },
    setImages: (state, data) => { state.profileImages = data },
    setAllClients: (state, data) => { state.globalAllClients = data }
  },
  actions: {
    getAllClients({ commit }) {
      return fetchClientsList().then(res => {
        commit('setAllClients', res.data)
      })
    },
    getUser({ commit }) {
      return userData()
        .then(res => {
          commit('setUserData', res.data)
        })
    },
    getSelectTags({ commit }) {
      return userTags()
        .then(res => {
          commit('setSelectTags', res.data)
        })
    },
    getAboutTags({ commit }) {
      return aboutTags()
        .then(res => {
          commit('setAboutTags', res.data)
        })
    },
    getRecommends({ commit }) {
      return recommendList()
        .then(res => {
          commit('setRecommends', res.data)
        })
    },
    getPositions({ commit }) {
      return positionList()
        .then(res => {
          commit('setPositions', res.data)
        })
    },
    getUserImages({ commit }) {
      return userImages()
        .then(res => {
          commit('setImages', res.data)
        })
    }
  }
}

export default client_store
