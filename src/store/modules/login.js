const login = {
  state: {
    loginUser: {
      token: '',
      roles: null
    }
  },
  getters: {
    getLogin: state => state.loginUser,
    getToken: state => state.loginUser.token,
    getRole: state => state.loginUser.roles
  },
  mutations: {
    setUser: (state, data) => { state.loginUser = data }
  }
}

export default login
