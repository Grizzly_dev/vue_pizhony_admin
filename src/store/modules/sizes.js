import { shirtSize, trouserSize, shoeSize } from './../../api/demo_data/sizes'

const sizes = {
  state: {
    sizes: {
      shirts: [],
      trouser: [],
      shoes: []
    }
  },
  getters: {
    shirtSizes: state => state.sizes.shirts,
    trouserSizes: state => state.sizes.trouser,
    shoeSizes: state => state.sizes.shoes
  },
  mutations: {
    setShirtsSizes: (state, data) => { state.sizes.shirts = data },
    setTrouserSizes: (state, data) => { state.sizes.trouser = data },
    setShoesSizes: (state, data) => { state.sizes.shoes = data }
  },
  actions: {
    getShirtsSizes({ commit }) {
      return shirtSize()
        .then(res => {
          commit('setShirtsSizes', res.data)
        })
    },
    getTrousersSizes({ commit }) {
      return trouserSize()
        .then(res => {
          commit('setTrouserSizes', res.data)
        })
    },
    getShoesSizes({ commit }) {
      return shoeSize()
        .then(res => {
          commit('setShoesSizes', res.data)
        })
    }
  }
}

export default sizes
