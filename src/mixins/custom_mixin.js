export const userDataMixin = {
  methods: {
    arrToSelectFormat(arrData) {
      if (!arrData || !arrData.length) {
        // console.log(`%c wrong data format`, `background: orange;color:#000;`)
        return null
      }
      const arrToSelect = []

      arrData.map(item => {
        const formated = {}
        formated['value'] = item
        formated['title'] = item

        arrToSelect.push(formated)
      })

      return arrToSelect
    },
    hanleSizesObj(res) {
      const result = []
      res.data.map(sizeObj => {
        sizeObj.sizes.map(sizeArr => {
          if (!result.includes(sizeArr)) {
            result.push(sizeArr)
          }
        })
      })
      this.sizes = result.sort()
    },
    alertRequestSyccess() {
      this.$alert(' ', 'Создание успешно!', {
        confirmButtonText: 'OK'
      })
    },
    alertEditSuccess() {
      this.$alert(' ', 'Редактирование успешно!', {
        confirmButtonText: 'OK'
      })
    },
    alertRemoveSyccess() {
      this.$alert(' ', 'Удаление успешно!', {
        confirmButtonText: 'OK'
      })
    },
    alertRequestFail() {
      this.$alert('Ошибка запроса!', ':(', {
        confirmButtonText: 'OK'
      })
    }
  }
}
