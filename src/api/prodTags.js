import request from '@/utils/request'

export function fetchProductTags(query) {
  return request({
    url: '/prodtags/all',
    method: 'get',
    params: query
  })
}

export function fetchProdTag(idArr) {
  return request({
    url: '/prodtags/detail',
    method: 'get',
    params: { idArr }
  })
}
