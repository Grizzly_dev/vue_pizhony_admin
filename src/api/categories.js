import request from '@/utils/request'

export function fetchCategoriesList(query) {
  return request({
    // url: '/categories/all',
    url: 'http://grizzly-dev.site/api/categories',
    method: 'get',
    params: query
  })
}

export function fetchCategory(idArr) {
  return request({
    url: '/categories/detail',
    method: 'get',
    params: { idArr }
  })
}

export function fetchCategoryContent(id) {
  return request({
    url: '/categories/content',
    method: 'get',
    params: { id }
  })
}
