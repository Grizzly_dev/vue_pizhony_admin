import request from '@/utils/request'

export function fetchOrders(query) {
  // clean object:
  for (const prop in query) {
    if (query[prop] === null || query[prop] === undefined || query[prop] === '') {
      delete query[prop]
    }
  }
  // form url:

  return request({
    url: `http://grizzly-dev.site/api/orders`,
    method: 'get',
    params: query
  })
}

export function addOrder(data) {
  return request({
    url: 'http://grizzly-dev.site/api/orders/store',
    method: 'post',
    data
  })
}

export function deleteOrder(id) {
  return request({
    url: `http://grizzly-dev.site/api/orders/${id}/destroy`,
    method: 'DELETE'
  })
}

export function fetchStatuses() {
  return request({
    url: `http://grizzly-dev.site/api/orders/statuses`,
    method: 'get'
  })
}

export function fetchOrderHistory() {
  return request({
    url: `http://grizzly-dev.site/api/orders`,
    method: 'get'
  })
}

export function updateOrderState(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/orders/${id}/update`,
    method: 'PUT',
    data
  })
}
