import request from '@/utils/request'

export function fetchSettings() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/global',
    method: 'get'
  })
}

export function saveSettings(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/settings/global/${id}/update`,
    method: 'PUT',
    data
  })
}
