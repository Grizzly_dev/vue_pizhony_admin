import request from '@/utils/request'

export function fetchClientsList(query) {
  return request({
    // url: '/clients/list',
    url: 'http://grizzly-dev.site/api/profiles',
    method: 'get',
    params: query
  })
}

export function fetchClientsHistory(id) {
  return request({
    url: `http://grizzly-dev.site/api/profiles/${id}/order-history`,
    method: 'get'
  })
}

export function fetchClientsFroms() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/froms',
    method: 'get'
  })
}

export function removeClientsFroms(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/froms/${id}/destroy`,
    method: 'DELETE'
  })
}

export function fetchClientsFiltered(query) {
  return request({
    url: 'http://grizzly-dev.site/api/profiles',
    method: 'get',
    params: {
      search: query
    }
  })
}

export function fetchClient(id) {
  return request({
    url: `http://grizzly-dev.site/api/profiles/${id}`,
    method: 'get'
  })
}

export function fetchClientHistory(id) {
  return request({
    url: `http://grizzly-dev.site/api/orders/${id}`,
    method: 'get'
  })
}

export function createClient(data) {
  return request({
    url: 'http://grizzly-dev.site/api/profiles/store',
    method: 'post',
    data
  })
}

export function updateClient(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/profiles/${id}/update`,
    method: 'put',
    data
  })
}
