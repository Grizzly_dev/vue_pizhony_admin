import request from '@/utils/request'

export function fetchBlogArticlesAll(query) {
  return request({
    url: 'http://grizzly-dev.site/api/blog',
    method: 'get',
    params: query
  })
}

export function fetchBlogArticle(id) {
  return request({
    url: `http://grizzly-dev.site/api/blog/${id}`,
    method: 'get'
  })
}

export function fetchBlogArticleCreate(data) {
  return request({
    url: 'http://grizzly-dev.site/api/blog/store',
    method: 'post',
    data
  })
}

export function fetchBlogArticleUpdate(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/blog/${id}/update`,
    method: 'put',
    data
  })
}

export function fetchRemoveBlogArticleMainImg(post_id) {
  return request({
    url: 'http://grizzly-dev.site/api/blog/post-images/main/destroy',
    method: 'delete',
    params: { post_id }
  })
}

export function fetchRemoveBlogArticlePreviewImg(id) {
  return request({
    url: 'http://grizzly-dev.site/api/blog/post-images/preview/destroy',
    method: 'delete',
    params: { id }
  })
}
