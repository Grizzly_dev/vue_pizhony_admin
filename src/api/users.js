import request from '@/utils/request'

// export function fetchClientsList(query) {
//   return request({
//     // url: '/clients/list',
//     url: 'http://grizzly-dev.site/api/profiles',
//     method: 'get',
//     params: query
//   })
// }
//
// export function fetchClientsHistory(id) {
//   return request({
//     url: `http://grizzly-dev.site/api/profiles/${id}/order-history`,
//     method: 'get'
//   })
// }

export function getManagers() {
  return request({
    url: 'http://grizzly-dev.site/api/users',
    method: 'get'
  })
}

export function createUser(data) {
  return request({
    url: 'http://grizzly-dev.site/api/auth/register',
    method: 'post',
    data
  })
}
//
// export function updateClient(id, data) {
//   return request({
//     url: `http://grizzly-dev.site/api/profiles/${id}/update`,
//     method: 'put',
//     data
//   })
// }
