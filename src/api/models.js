import request from '@/utils/request'

export function fetchModels() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/models',
    method: 'get'
  })
}

export function removeModel(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/models/${id}/destroy`,
    method: 'DELETE'
  })
}
