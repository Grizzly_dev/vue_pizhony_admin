import request from '@/utils/request'

export function fetchSizesAll(query) {
  return request({
    url: '/sizequantity/all',
    method: 'get',
    params: query
  })
}

export function fetchSizesQuantity(idArr) {
  return request({
    url: '/sizequantity/detail',
    method: 'get',
    params: { idArr }
  })
}
