import request from '@/utils/request'

export function fetchFinances(query) {
  return request({
    url: 'http://grizzly-dev.site/api/finance',
    method: 'get',
    params: query
  })
}
