import request from '@/utils/request'

export function fetchСCountries() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/countries',
    method: 'get'
  })
}

export function deleteCountry(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/countries/${id}/destroy`,
    method: 'DELETE'
  })
}

export function updateCountry(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/settings/countries/${id}/update`,
    method: 'PUT',
    data
  })
}

