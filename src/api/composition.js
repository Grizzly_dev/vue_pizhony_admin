import request from '@/utils/request'

export function fetchComposition() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/compositions',
    method: 'get'
  })
}

export function deleteComposition(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/compositions/${id}/destroy`,
    method: 'DELETE'
  })
}

export function updateComp(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/settings/compositions/${id}/update`,
    method: 'PUT',
    data
  })
}
