import request from '@/utils/request'

export function fetchSubcategoriesList() {
  return request({
    // url: '/subcategories/all',
    url: 'http://grizzly-dev.site/api/categories/sub-categories',
    method: 'get'
  })
}

export function fetchSubcategory(idArr) {
  return request({
    url: '/subcategories/detail',
    method: 'get',
    params: { idArr }
  })
}
