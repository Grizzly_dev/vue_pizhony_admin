import request from '@/utils/request'

export function fetchSeasons() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/seasons',
    method: 'get'
  })
}

export function deleteSeason(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/seasons/${id}/destroy`,
    method: 'DELETE'
  })
}

export function updateSeason(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/settings/seasons/${id}/update`,
    method: 'PUT',
    data
  })
}
