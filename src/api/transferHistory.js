import request from '@/utils/request'

export function fetchTransferList(query) {
  return request({
    url: '/transfer/all',
    method: 'get',
    params: query
  })
}
