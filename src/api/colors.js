import request from '@/utils/request'

export function fetchColors() {
  return request({
    // url: '/colors/all',
    url: 'http://grizzly-dev.site/api/settings/colors',
    method: 'get'
  })
}

export function deleteColors(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/colors/${id}/destroy`,
    method: 'DELETE'
  })
}

export function updateColor(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/settings/colors/${id}/update`,
    method: 'PUT',
    data
  })
}
