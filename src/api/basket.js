// // объявляем функию
// function setBasket(prods) {
//   // const prods = [
//   //   {
//   //     article: "7869",
//   //     category_id: 2,
//   //     color: 7,
//   //     color_title: "green",
//   //     comment: "Some data",
//   //     price: 4213,
//   //     product_id: 2,
//   //     quantity: 3,
//   //     size: "1",
//   //     title: "Product 2"
//   //   },
//   //   {
//   //     article: "7869",
//   //     category_id: 2,
//   //     color: 7,
//   //     color_title: "green",
//   //     comment: "Some data",
//   //     price: 4213,
//   //     product_id: 2,
//   //     quantity: 2,
//   //     size: "undefined",
//   //     title: "Product 2",
//   //   },
//   //   {
//   //     article: "6995",
//   //     category_id: 1,
//   //     color: 14,
//   //     color_title: "green",
//   //     comment: "Some data",
//   //     price: 4213,
//   //     product_id: 1,
//   //     quantity: 5,
//   //     size: "1",
//   //     title: "Product 1"
//   //   }
//   // ]
//   console.log(`%c run setBasket`, 'background: purple;color:#fff;')
//   console.log(prods)
//   const $basketWrap = $('.minicart-widget .minicart-content')
//   $basketWrap.html('')
//
//   const prodsBuilder = (prod) => {
//     return `
//                 <div class="single-cart-item row no-gutters">
//                     <div class="item-img"><img src="assets/template/img/category-img3.png"></div>
//                     <div class="item-info">
//                         <div class="item-name">${prod.title}</div>
//                         <div class="item-option">Кол-во: ${prod.quantity} - ${prod.price * prod.quantity} BYN</div>
//                         <div class="item-option">Цвет: ${prod.color_title}</div>
//                         <div class="item-option">Размер: ${prod.size}</div>
//                     </div>
//                     <a href="index.html#" class="item-remove"></a>
//                 </div>
//             `
//   }
//
//   for (const prod of prods) {
//     console.log(prodsBuilder(prod))
//     $basketWrap.append(prodsBuilder(prod))
//   }
// }
// // вызываем где надо с продуктами из сессии
// setBasket(session_prods)
