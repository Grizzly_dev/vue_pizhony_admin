import request from '@/utils/request'

export function fetchShops() {
  return request({
    url: 'http://grizzly-dev.site/api/shops',
    method: 'get'
  })
}

export function removeShop(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/shops/${id}/destroy`,
    method: 'DELETE'
  })
}
