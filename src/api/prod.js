import request from '@/utils/request'

export function fetchProductsList(query) {
  return request({
    url: 'http://grizzly-dev.site/api/products',
    method: 'get',
    params: query
  })
}

export function fetchProduct(id) {
  return request({
    // url: '/products/detail',
    url: `http://grizzly-dev.site/api/products/${id}`,
    method: 'get'
  })
}

export function addProd(data) {
  return request({
    url: 'http://grizzly-dev.site/api/products/store',
    method: 'post',
    data
  })
}

export function updateProduct(id, data) {
  return request({
    // url: '/products/detail',
    url: `http://grizzly-dev.site/api/products/${id}/update`,
    method: 'PUT',
    data
  })
}

export function fetchProductsFiltered(query) {
  return request({
    url: 'http://grizzly-dev.site/api/products',
    method: 'get',
    params: {
      search: query
    }
  })
}

export function fetchProductByShopID(id) {
  return request({
    url: 'http://grizzly-dev.site/api/products',
    method: 'get',
    params: { id }
  })
}

export function fetchProductByName(name) {
  return request({
    url: '/products/getbyname',
    method: 'get',
    params: { name }
  })
}

export function fetchProductByArticle(article) {
  return request({
    url: '/products/article',
    method: 'get',
    params: { article }
  })
}

export function fetchRemoveMainImg(product_id) {
  return request({
    url: 'http://grizzly-dev.site/api/media/product-images/main/destroy',
    method: 'delete',
    params: { product_id }
  })
}

export function fetchRemoveOtherImg(id) {
  return request({
    url: 'http://grizzly-dev.site/api/media/product-images/multiple/destroy',
    method: 'delete',
    params: { id }
  })
}

export function fetchRemoveBarcode(product_id) {
  return request({
    url: 'http://grizzly-dev.site/api/media/product-images/barcode/destroy',
    method: 'delete',
    params: { product_id }
  })
}
