import request from '@/utils/request'

export function fetchBrands() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/brands',
    method: 'get'
  })
}

export function removeBrand(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/brands/${id}/destroy`,
    method: 'DELETE'
  })
}
