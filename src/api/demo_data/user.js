export const userData = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        data: {
          'userID': '1742',
          'card_id': '0001',
          'created': '2019-07-31T21:00:00.000Z',
          'sex': 'male',
          'total': '985',
          'discount': '0',
          'cashback': '3',
          'country': 'Cntry',
          'city': 'City',
          'postIdx': '1242312',
          'surname': 'Surname',
          'name': 'Name',
          'postAddr': 'post 492',
          'recommend': ['user 3 | +375 33 667-54-33'],
          'wedd_date': '2019-08-14T21:00:00.000Z',
          'tags_main': ['user tag 2', 'another tag'],
          'tags_client': ['another about'],
          'rate': 4,
          'phone': '+375 33 333-33-33',
          'email': 'test@gmail.com',
          'position': ['pos 22'],
          'trouserSize': ['XL', 'M'],
          'images': {
            'main_img': 'https://picsum.photos/1024/620/?image=53',
            other: [
              'https://picsum.photos/1024/620/?image=55',
              'https://picsum.photos/1024/620/?image=56',
              'https://picsum.photos/1024/620/?image=57',
              'https://picsum.photos/1024/620/?image=58'
            ]
          }
        }
      })
    }, 98)
  })
}
