export const orderHistory = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          {
            id: 1,
            card_id: 111,
            sale: 5,
            cashback: 10,
            prods: [
              {
                id: 1,
                title: 'Рубашка белая',
                size: '44',
                price: '114'
              },
              {
                id: 2,
                title: 'Брюки',
                size: '44',
                price: '175'
              }
            ],
            total: 289,
            date: '22.11.2018',
            manager: 'Max',
            shop: 'ТЦ силуэт'
          },
          {
            id: 2,
            card_id: 111,
            sale: 5,
            cashback: 12,
            prods: [
              {
                id: 1,
                title: 'Броги',
                size: '39',
                price: '100'
              },
              {
                id: 2,
                title: 'Бабочка',
                size: '2',
                price: '100'
              }
            ],
            total: 200,
            date: '10.12.2018',
            manager: 'Dmitry',
            shop: 'ТЦ силуэт'
          },
          {
            id: 3,
            card_id: 111,
            sale: 5,
            cashback: 13,
            prods: [
              {
                id: 1,
                title: 'Костюм тройка',
                size: '44',
                price: '380'
              },
              {
                id: 2,
                title: 'Бабочка',
                size: '2',
                price: '102'
              }
            ],
            total: 482,
            date: '10.12.2018',
            manager: 'Max',
            shop: 'Concept Store'
          }
        ]
      })
    }, 141)
  })
}
