export const shirtSize = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          '38',
          '43',
          '12',
          '44',
          '45',
          '46'
        ]
      })
    }, 121)
  })
}

export const trouserSize = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          'A12',
          'A2312',
          'XL',
          'M',
          'XS'
        ]
      })
    }, 186)
  })
}

export const shoeSize = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          '38',
          '39',
          '40',
          '41',
          '42',
          '43',
          '44',
          '45'
        ]
      })
    }, 213)
  })
}
