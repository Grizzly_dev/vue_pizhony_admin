export const userTags = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          'tag1',
          'user tag 2',
          'another tag',
          'tag 4'
        ]
      })
    }, 231)
  })
}

export const aboutTags = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          'about',
          'user about 2',
          'another about',
          'about 4'
        ]
      })
    }, 142)
  })
}

export const recommendList = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          'user 1 | +375 33 333-33-33',
          'user 2 | +375 44 231-43-43',
          'user 3 | +375 33 667-54-33',
          'user 4 | +375 17 078-43-12',
          'user 5 | +375 33 453-53-87',
          'user 6 | +375 25 338-56-56'
        ]
      })
    }, 121)
  })
}

export const positionList = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: [
          'pos 1',
          'pos 22',
          'pos 3',
          'pos 44',
          'pos 5'
        ]
      })
    }, 312)
  })
}

export const userImages = (request) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // if (!request) resolve([])
      resolve({
        data: {
          main_img: 'https://picsum.photos/1024/620/?image=53',
          other: [
            'https://picsum.photos/1024/620/?image=55',
            'https://picsum.photos/1024/620/?image=56',
            'https://picsum.photos/1024/620/?image=57',
            'https://picsum.photos/1024/620/?image=58'
          ]
        }
      })
    }, 111)
  })
}
