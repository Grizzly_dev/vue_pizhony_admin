import request from '@/utils/request'

export function fetchPreorderList(query) {
  return request({
    url: `http://grizzly-dev.site/api/preorders`,
    method: 'get'
    // params: query
  })
}
