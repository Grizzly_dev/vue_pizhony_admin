import axios from 'axios'

const request = axios.create({
  baseURL: process.env.API_BASE,
  withCredentials: true
})

const decomposeToArray = (response, property) => {
  return {
    data: response.data._embedded[property]
  }
}

export { request, decomposeToArray }
