import request from '@/utils/request'

export function storeProdStock(data) {
  return request({
    url: 'http://grizzly-dev.site/api/products/stock/store',
    method: 'POST',
    data
  })
}

export function updProdStock(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/products/stock/${id}/update`,
    method: 'PUT',
    data
  })
}
