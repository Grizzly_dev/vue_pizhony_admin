import request from '@/utils/request'

export function fetchAllTags() {
  return request({
    url: `http://grizzly-dev.site/api/settings/tags`,
    method: 'get'
  })
}

export function fetchTags(type) {
  return request({
    url: `http://grizzly-dev.site/api/settings/tags/${type}`,
    method: 'get'
  })
}

export function removeTag(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/tags/${id}/destroy`,
    method: 'DELETE'
  })
}
