import request from '@/utils/request'

export function fetchBlogTags(query) {
  return request({
    url: 'http://grizzly-dev.site/api/settings/tags',
    method: 'get',
    params: query
  })
}
