import request from '@/utils/request'

export function fetchFactories() {
  return request({
    // url: '/factories/all',
    url: 'http://grizzly-dev.site/api/settings/factories',
    method: 'get'
  })
}

export function removeFactory(id) {
  return request({
    url: `http://grizzly-dev.site/api/settings/factories/${id}/destroy`,
    method: 'DELETE'
  })
}
