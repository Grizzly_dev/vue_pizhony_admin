import request from '@/utils/request'

export function fetchAllSizes() {
  return request({
    url: 'http://grizzly-dev.site/api/settings/sizes',
    method: 'get'
  })
}

export function fetchSizeDetail(idArr) {
  return request({
    url: '/sizes/detail',
    method: 'get',
    params: { idArr }
  })
}

export function updateSizes(id, data) {
  return request({
    url: `http://grizzly-dev.site/api/settings/sizes/${id}/update`,
    method: 'PUT',
    data
  })
}
